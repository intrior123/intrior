Intrior - Personalise Your Space

## About Intrior
Intrior is the [best interior designer in Thane, Kalyan, Dombivli and Mumbai](https://intrior.com). We offer Interior Design Consultancy Services, Execution Services, Premium Modular Kitchens and a great range of Customised Furniture in and around Mumbai. We also offer personalised [interior designing services in Thane](https://intrior.com/best-interior-designer-in-thane.html).

18+ YEARS EXPERIENCE

Intrior was founded by Mr Rajesh Bhosale in 2002. Who is a professional Architect. The team understands that each project is unique and has its own character and requirements. Right from concept design to execution, we believe in making the process flawless, hassle-free and enjoyable for our clients.


450+ HAPPY CUSTOMERS

Our google reviews prove that we are the best interior designers in Thane, Navi Mumbai, Mumbai and Kalyan-Dombivli. It has been our constant endeavor to stay in the forefront of Interior Design believing that designs are simply an integration of one's dream. This philosophy drives the firm to contour what is in the mind and deliver dreams through a strong backbone of planning and implementation exercise.


45-DAY EXECUTION OR WE PAY RENT

Our project managers are involved throughout every project ensuring successful execution and delivery within the agreed timescale and budget. This guarantees that the project is delivered to meet, and often exceeds your expectations .We never ever delay a project as we have our SOP and Bar chart made for every project.


FACTORY FINISHED LATEST PRODUCTS

Intrior owns its own factory with the latest German Machines for finishes. This is how we can keep quality control. Each product undergoes rigorous quality checks to ensure its longevity. We deal with a variety of complex processes combining various materials and techniques to ensure that a product is made with the latest technology and under the able guidance of our craftsmen.


5+ YRS WARRANTY

Intrior provide manufacturer's warranty on Residential furniture such as Wardrobe, Modular Kitchen, Bed, Storages, with BWR Ply. INTRIOR is the only branded player that is offering a warranty for a period of upto one year on on-site services including painting, gypsum false ceiling, electrical and plumbing work. Our 5-year warranty is one of a kind wherein we offer you an assurance that the furniture we make is sturdy and timeless. It’s built to last. We are currently the only brand in the country to offer an unmatched 5 year warranty on home and office furniture. We also provide 5 yrs replacement warranty for hardware.


FREE INTERIOR CONSULTANCY AND 3D VIEWS

Everyone wants to enjoy the one spot on earth that's really theirs. Team intrior can look at a blank space and know upfront how to evenly weight the practicalities of life and objects and personal style in one design. Hiring an interior designer is too expensive for most people. We believe to give that personal styled space is everyone's right. So we provide interior designing and (3d views for premium budgets) free of cost.
